#======CONST=======
DISTANCE="700m"
VEL="3kmph"

#===========================================
#===========================================
reset
set title "(Downlink) Distance ".DISTANCE." ".VEL.": Delays, queue size, discrete queuing delay"
set key inside top right box
set xlabel "Time (s)"
#set xtic 10
set ylabel "Delay (ms)"
set y2label "Queue size (bytes)"
set output "tcp-averaged-dl.svg"
set y2tics nomirror tc lt 3
#set y2range [0:150000]
#set yrange [0:1200]



set terminal svg

IGNORE_BELOW=0
filter(x)=(x>IGNORE_BELOW)?(x):(1/0)

IGNORE_ABOVE=100
filter_h(x)=(x<=IGNORE_ABOVE)?(x):(1/0)

a=0
c=0
cum_avg(x)=(a=a+x,c=c+1,a/c)

plot "tcp-queue-averaged-dl.txt" using (filter_h($1/1000)):2 title "Average queuing delay" with lines,\
"tcp-queue-averaged-dl.txt" using (filter_h($1/1000)):3 title "Average queue size" axis x2y2 with lines,\
"tcp_lost_dl.txt" using ($1/1000):(100) title "TCP-lost",\
"tcp_buffer_discarded_dl.txt" using ($1/1000):(10) title "buffer discarded",\
"tcp-queue-dl.txt" using (filter_h($1/1000)):6 title "Queuing delay" pt 0 lc 4,\
"tcp-queue-dl.txt" using (filter_h($1/1000)):4 title "Queue size" pt 0 lc 6 axis x2y2

 
#==========================================
#==========================================



#======== d=700,v=3 ========	
#===========================================
#===========================================
reset
set title "(Downlink) Distance ".DISTANCE.", ".VEL." : Uplink UDP"
set key inside top right box
set xlabel "Time (s)"
#set xtic 10
set ylabel "Delay (ms)"
set y2label "Queue size (bytes)"
set output "udp-dl.svg"
set y2tics nomirror tc lt 2
#set y2range [0:150000]
#set yrange [0:1200]



set terminal svg

IGNORE_BELOW=0
filter(x)=(x>IGNORE_BELOW)?(x):(1/0)

IGNORE_ABOVE=100
filter_h(x)=(x<=IGNORE_ABOVE)?(x):(1/0)

a=0
c=0
cum_avg(x)=(a=a+x,c=c+1,a/c)

plot "udp-queue-dl.txt" using (filter_h($1/1000)):6 title "Queuing delay" with lines,\
"udp-queue-dl.txt" using (filter_h($1/1000)):4 title "Queue size" with lines axis x2y2
#"udp-put-dl.txt" using (filter_h($1/1000)):3 title "Udp put" pt 0 lc 1 axis x2y2 
#==========================================


#==========================================
#======== d=700,v=3 ========
reset
set title "(Downlink) Comparing Rates: ".DISTANCE.", ".VEL
set key inside bottom right box
set xlabel "Time (s)"
#set xtic 10
#set ylabel "Delay (ms)"
#set yrange [0:2000]
#set xrange [0:20]
set ylabel "Throughput (kbps)"
set output "tcp-delay-compare-dl.svg"
#set y2tics nomirror tc lt 3

cnt=0
filter(x)=(x=(x<cnt)?(1/0):x,cnt=x)
filter_y(x)=(x>40000 || x < 100)?(1/0):x
filter_y_tcp(x)=(x>10000 || x < 100)?(1/0):x
set terminal svg


plot "udp-put-dl.txt" using (filter($1)/1000):(filter_y($3)) title "Radio bandwidth" with lines,\
"tcp-put-dl.txt" using (filter($1)/1000):(filter_y_tcp($3)) title "TCP throughput (Rx rate)" with lines,\
"tcp-put-dl.txt" using (filter($1)/1000):(filter_y_tcp($11)) title "TCP Tx rate" with lines,\
"tcp_lost_dl.txt" using ($1/1000):(100) title "TCP-lost",\
"tcp_buffer_discarded_dl.txt" using ($1/1000):(10) title "buffer discarded" 



#======== d=700,v=3 ========	
#===========================================
#===========================================
reset
set title "(Donwlink) Congestion window and BDP (Bandwidth Delay Product) ".DISTANCE.", ".VEL.": Uplink UDP"
set key inside top right box
set xlabel "Time (s)"
#set xtic 10
set ylabel "value (bytes)"
#set y2label "Queue size (bytes)"
set output "cwnd-dl.svg"
#set y2tics nomirror tc lt 2
#set y2range [0:150000]
#set yrange [0:1200]



set terminal svg

IGNORE_BELOW=0
filter(x)=(x>IGNORE_BELOW)?(x):(1/0)

IGNORE_ABOVE=100
filter_h(x)=(x<=IGNORE_ABOVE)?(x):(1/0)

a=0
c=0
cum_avg(x)=(a=a+x,c=c+1,a/c)

plot "cwnd-dl.txt" using (filter_h($1/1000)):($5*577*8) title "cwnd" with lines,\
"udp-put-dl.txt" using (filter_h($1/1000)):($3*1000*0.015) title "BDP" with lines,\
"tcp_lost_dl.txt" using ($1/1000):(0) title "TCP lost",\
"harq-dl.txt" using ($6/1000):(100) title "HARQ retransmittion"
#"tcp_buffer_discarded_dl.txt" using ($1/1000):(10) title "buffer discarded" 
#"udp-put.txt" using (filter_h($1/1000)):3 title "Udp put" pt 0 lc 1 axis x2y2 
#==========================================
#==========================================

