#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['ns3-dev-e2e-fading-dl-debug', 'ns3-dev-e2e-fading-ul-mobile-debug', 'ns3-dev-e2e-fading-ul-debug', 'ns3-dev-e2e-flow-debug', 'ns3-dev-e2e-merge-debug', 'ns3-dev-e2e-tcp-dl-debug', 'ns3-dev-e2e-tcp-ul-debug', 'ns3-dev-e2e-udp-dl-debug', 'ns3-dev-e2e-udp-ul-debug', 'ns3-dev-e2e-ul-debug', 'ns3-dev-e2e-debug', 'ns3-dev-lena-simple-epc-debug', 'ns3-dev-scratch-simulator-debug', 'ns3-dev-subdir-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

