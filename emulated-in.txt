value /$ns3::NodeListPriv/NodeList/0/$ns3::Node/DeviceList/0/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue/MaxPackets "100"
value /$ns3::NodeListPriv/NodeList/0/$ns3::Node/DeviceList/0/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue/MaxBytes "6553500"

value /$ns3::NodeListPriv/NodeList/1/$ns3::Node/DeviceList/0/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue/MaxPackets "100"
value /$ns3::NodeListPriv/NodeList/1/$ns3::Node/DeviceList/0/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue/MaxBytes "6553500"

value /$ns3::NodeListPriv/NodeList/1/$ns3::Node/DeviceList/1/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue/MaxPackets "10000"
value /$ns3::NodeListPriv/NodeList/1/$ns3::Node/DeviceList/1/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue/MaxBytes "65535000"

value /$ns3::NodeListPriv/NodeList/2/$ns3::Node/DeviceList/0/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue/MaxPackets "100"
value /$ns3::NodeListPriv/NodeList/2/$ns3::Node/DeviceList/0/$ns3::PointToPointNetDevice/TxQueue/$ns3::DropTailQueue/MaxBytes "6553500"

default ns3::DropTailQueue::Mode "QUEUE_MODE_PACKETS"
default ns3::DropTailQueue::MaxPackets "100"
default ns3::DropTailQueue::MaxBytes "6553500"

default ns3::TcpL4Protocol::SocketType "ns3::TcpTahoe"

